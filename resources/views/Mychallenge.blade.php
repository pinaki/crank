@extends('layouts.app')
@section('content')
<div class="container">
<div class="text-right" style="padding-top:2%">
<a href="{{route('ncpage')}}" class="btn btn-primary" style="line-height: 1;">Create New Challenge </a>


</div>
<strong>Challenges</strong>
<div class=" text-center">
<br> 
@foreach($challenges as $challenge)
<div class="card">
<div class="row">
<div class="col-md-6 text-left" style="padding-top:10px;padding-left:30px;padding-bottom:15px">
<b>{{$challenge->cname}}</b><br>
<b>{{$challenge->desc}}</b>
</div>
<div class="col-md-6 text-center">
<br>
<a href="{{route('edit',['id'=>$challenge->id])}}" class="btn btn-primary">Update</a> &nbsp;&nbsp;
<a href="{{route('del',['id'=>$challenge->id])}}" class="btn btn-primary">delete</a>

</div>

</div> 
</div>
<br>

@endforeach

</div>
</div>
@endsection