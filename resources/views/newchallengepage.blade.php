@extends('layouts.app')
@section('content')


<form method="POST" action="{{route('challenge')}}">
    @csrf
<div class="container">
   
    <h2> Create Challenge </h2>
     <p class="aside block-margin margin-large"> <i>Get started by providing the initial details needed to create a challenge.
         </i></p>
         
         
          <div class="row " style="padding-bottom: 30px;">
    <label class=" col-md-3"  for="challengename"><b>Challenge Name:</b> </label>
    <div class="col-md-9">
    <input class="form-control form-control-lg" id="inputsm" type="text" name="challengename" class="span12" required>
    </div>
  </div>
         
         
         
          <div class="row ">
    <label class=" col-md-3"  for="description"><b>Description:</b></label>
    <div class="col-md-9">
    <textarea rows="2" cols="100" id="preview"  class="form-control form-control-lg" class="description span16" placeholder="Write a short Summary about the challnege" name="description"></textarea>
     <small class="description pull-left sub-help"> Charaters left :140</small>
    </div>
  </div>
         
         
          <div class="row " style="padding-bottom: 30px;">
              <label for="name" class=" col-md-3" > <b>Problem Statement </b> </label>
    <div class="col-md-9">
    <textarea name="problemstatement" class="ckeditor block  profile-input">  </textarea>
    </div>
  </div>
         
         
          <div class="row " style="padding-bottom: 30px;">
              <label class=" col-md-3"  for="email"> <b> Input Format </b></label>
    <div class="col-md-9">
    <textarea name="inputformat" class="ckeditor block  profile-input">  </textarea>
    </div>
  </div>
         
         
         
         <div class="row " style="padding-bottom: 30px;">
    <label class=" col-md-3"  for="email"><b> Constraints </b> </label>
    <div class="col-md-9">
    <textarea name="constraints" class="ckeditor block  profile-input">  </textarea>
    </div>
  </div>
    
          <div class="row ">
    <label class=" col-md-3"  for="email"><b> Output Format </b> </label>
    <div class="col-md-9">
    <textarea name="outputformat" class="ckeditor block  profile-input">  </textarea>
    </div>
  </div>
         
         
  <div class="row ">
    <label class=" col-md-3"  for="email"><b> Tags </b> </label>
    <div class="col-md-9">
        <input name="tags" class="form-control form-control-lg" id="inputsm" type="text" class="span12">
    </div>
  </div>
          <div align="right" style="padding-bottom:20px">
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
              <hr style="background-color: black;">
              <button type="submit"  class="btn btn-success"><i class="fa fa-check">Save</i></button>
          </div>
          
            
</div>
        </form>
         
    </body>
    <style>
            .row{
               padding-bottom: 30px;
            }
            .row{
               padding-bottom: 30px;
            }
        </style>
<script src="https://cdn.ckeditor.com/4.10.0/standard-all/ckeditor.js"></script>

<script>
var allEditors = document.querySelectorAll('.ckeditor');
for (var i = 0; i < allEditors.length; ++i) {
CKEDITOR.replace(allEditors[i].attr('id'));
}
</script>





@endsection

