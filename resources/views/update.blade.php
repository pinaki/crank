@extends('layouts.app')

@section('content')


 <form method="POST" action="{{route('update',['id'=>$challenges->id])}}">
    @csrf
<div class="container">
   
    <h2> Create Challenge </h2>
     <p class="aside block-margin margin-large"> <i>Get started by providing the initial details needed to create a challenge.
         </i></p>
         
         
          <div class="row " style="padding-bottom: 30px;">
    <label class=" col-md-3"  for="challengename"><b>Challenge Name:</b> </label>
    <div class="col-md-9">
    <input class="form-control form-control-lg" id="inputsm" type="text" name="challengename" 
    value="{{$challenges->cname}}" class="span12">
    </div>
  </div>
         
         
         
          <div class="row ">
    <label class=" col-md-3"  for="description"><b>Description:</b></label>
    <div class="col-md-9">
        
    <textarea rows="2" cols="100" id="preview"  class="form-control form-control-lg" class="description span16"  
  name="description">{{$challenges->desc}}</textarea>
     <small class="description pull-left sub-help"> Characters left :140</small>
    </div>
  </div>
         
         
          <div class="row " style="padding-bottom: 30px;">
              <label for="name" class=" col-md-3" > <b>Problem Statement </b> </label>
    <div class="col-md-9">
    <textarea name="problemstatement"  id="problemstatement" class="ckeditor block  profile-input">  </textarea>
    </div>
  </div>
         
         
          <div class="row " style="padding-bottom: 30px;">
              <label class=" col-md-3"  for="input"> <b> Input Format </b></label>
    <div class="col-md-9">
    <textarea name="inputformat" class="ckeditor block  profile-input">  </textarea>
    </div>
  </div>
         
         
         
         <div class="row " style="padding-bottom: 30px;">
    <label class=" col-md-3"  for="constraints"><b> Constraints </b> </label>
    <div class="col-md-9">
    <textarea name="constraints" class="ckeditor block  profile-input">  </textarea>
    </div>
  </div>
    
          <div class="row " style="padding-bottom: 30px;">
    <label class=" col-md-3"  for="output"><b> Output Format </b> </label>
    <div class="col-md-9">
    <textarea name="outputformat" class="ckeditor block  profile-input">  </textarea>
    </div>
  </div>
         
         
  <div class="row ">
    <label class=" col-md-3"  for="tags"><b> Tags </b> </label>
    <div class="col-md-9">
        <input name="tags" value="{{$challenges->tags}} "class="form-control form-control-lg" id="inputsm" type="text" class="span12">
    </div>
  </div>
          <div align="right">
              <hr style="background-color: black">
           
                  
              <button type="submit">Save</button>
              
          </div>
</div>

        
         

    <script src="https://cdn.ckeditor.com/4.10.0/standard-all/ckeditor.js"></script>
       
<script>
function codeAddress() {

var object = {!! json_encode($challenges->toArray()) !!};

var problemstatement=object['statement'];
var editor = CKEDITOR.instances['problemstatement'];
editor.setData(problemstatement);

var inputformat=object['ipformat'];
var editor = CKEDITOR.instances['inputformat'];
editor.setData(inputformat);

var constraints=object['constraints'];
var editor = CKEDITOR.instances['constraints'];
editor.setData(constraints);

var outputformat=object['opformat'];
var editor = CKEDITOR.instances['outputformat'];
editor.setData(outputformat);
}
window.onload = codeAddress;
</script>

</form>
</html>
@endsection

