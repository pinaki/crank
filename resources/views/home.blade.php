@extends('layouts.app')

@section('content')

   
<div class="container text-center" style="  padding-top:10px;"><div>
    <!-- <div class="container text-left">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <span style="color:Red"><b>60 More points <span style="color:green">for next &nbsp;<i class="fa fa-star fa-lg" aria-hidden="true" style="color:orange"></i></span></b></span>
        <div class="progress" style="width:25%">
            <div class="progress-bar bg-success progress-bar-striped" style="width:30%"></div>
    </div>
    <span><b>Rank:27813&nbsp;&nbsp;Points:40/100</b></span>
    
    <div class="container text-right">
    <a href="{{route('subpage')}}" class="btn btn-info">My Submissions </a>      
    <a href="{{route('ncpage')}}" class="btn btn-info">Create New Challenge </a>                           
    </div>
    </div> -->
    <div class="row">
     <div class="col-md-6 text-left">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
           <span style="color:Red"><b>60 More points <span style="color:green">for next &nbsp;<i class="fa fa-star fa-lg" aria-hidden="true" style="color:orange"></i></span></b></span>
            <div class="progress" style="width:40%">
            <div class="progress-bar bg-success progress-bar-striped" style="width:30%"></div>
            </div>
            <span><b>Rank:27813&nbsp;&nbsp;Points:40/100</b></span>

    </div>
    <div class="col-md-6 text-right">
            <br>
            <a href="{{route('subpage')}}" class="btn btn-primary" style="line-height: 1;">My Submissions </a>     
            <a href="{{route('ncpage')}}" class="btn btn-primary" style="line-height: 1;">Create New Challenge </a>
    </div>
    </div>
    <!-- <hr> -->&nbsp;
    <div class="text-left">
    <h4><strong>Challenges:</strong></h4>
    </div>
<br>
    <div class="card">
        <div class="row">
            <div class="col-md-6 text-left"><br>
           {{}}
            </div>
            <div class="col-md-6 text-center">
            <br>
            <a href="{{route('')}}" class="btn btn-primary">Solve Challenge</a>
            </div>
    </div> 
    </div>
    <br>
    <div class="card">
        <div class="row">
            <div class="col-md-6 text-left"><br>
            <h5> &nbsp;  &nbsp;Matrix Operations</h5>
            <h6> &nbsp; &nbsp; syMax Score: 6 Success Rate: 99.74%</h6><br>
            </div>
            <div class="col-md-6 text-center">
            <br>
            <a href="{{route('dis')}}" class="btn btn-primary">Solve Challenge</a>
            </div>
    </div>  
    
   </div>
   <br>
   <div class="card">
        <div class="row">
            <div class="col-md-6 text-left"><br>
            <h5> &nbsp;  &nbsp;String Operations</h5>
            <h6> &nbsp; &nbsp; syMax Score: 6 Success Rate: 99.74%</h6><br>
            </div>
            <div class="col-md-6 text-center">
            <br>
            <a href="{{route('dis')}}" class="btn btn-primary">Solve Challenge</a>
            </div>
    </div> 
    </div>
    </div>
</div>

@endsection