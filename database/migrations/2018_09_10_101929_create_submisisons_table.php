<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmisisonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submisisons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid')->unsigned()->index();
            $table->foreign('uid')->references('id')->on('users')->onDelete('CASCADE');
            $table->integer('cid')->unsigned()->index();
            $table->foreign('cid')->references('id')->on('challenges')->onDelete('CASCADE');
            $table->string('code');
            $table->string('cstatus');
            $table->string('rstatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('submisisons');
    }
}
