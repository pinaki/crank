<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChallengesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challenges', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('uid')->unsigned()->index();
            $table->foreign('uid')->references('id')->on('users')->onDelete('CASCADE');
            $table->string('cname');
            $table->string('desc');
            $table->string('statement');
            $table->string('ipformat');
            $table->string('constraints');
            $table->string('opformat');
            $table->string('tags');     
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challenges');
    }
}
