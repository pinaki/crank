<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Challenge;
class ChallengeController extends Controller
{
    public function store(Request $request)  
     {         
        $submit = new Challenge;
        $submit->cname = $request->get('challengename');
        $submit->desc = $request->get('description');
        $submit->statement = strip_tags($request->get('problemstatement'));
        $submit->ipformat = strip_tags($request->get('inputformat'));
        $submit->constraints = strip_tags($request->get('constraints'));
        $submit->opformat = strip_tags($request->get('outputformat'));
        $submit->tags = $request->get('tags');
        $submit->user_id=Auth::user()->id;
        $submit->save();
        return view('home');
   }
   public function show()
   {
    $userid=Auth::user()->id;
    $challenge=challenge::all();
    return view('home',['challenge'=>$challenge]);
   }
}
