<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\User;
use App\Challenge;
class SubmissionController extends Controller
{
    public function sub()
    {   
        $userid=Auth::user()->id;
        $submissions = User::find($userid)->submission;
      
        return view('submission',['subs'=>$submissions]);
    }
}
