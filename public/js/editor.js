
$(document).ready(function(){
var editor = ace.edit("editor");
editor.setTheme("ace/theme/monokai");
// editor.session.setMode("ace/mode/javascript");
// var textarea = $('textarea[name="editor"]').hide();
editor.getSession().setMode("ace/mode/javascript");


$('#mode').on('change', function(){
let newMode = $("#mode").val();
editor.getSession().setMode({
path: "ace/mode/" + newMode,
v: Date.now()});
});
});
